﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace FinalExam.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public IEnumerable<Place> Places { get; set; }
        public IEnumerable<Report> Reports { get; set; }
        public IEnumerable<ReportImage> Images { get; set; }
    }
}
