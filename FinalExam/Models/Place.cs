﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinalExam.Models
{
    public class Place
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Desription { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public string ApplicationUserId { get; set; }
        public IEnumerable<Report> Reports { get; set; }
        public string ImagePath { get; set; }
        public IEnumerable<ReportImage> Images { get; set; }
        public double AverageRaiting { get; set; }

    }
}
