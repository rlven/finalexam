﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FinalExam.Data;
using FinalExam.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.IO;
using FinalExam.Services;
using Microsoft.AspNetCore.Hosting;

namespace FinalExam.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _appEnvironment;
        private readonly FileUploadService _fileUploadService;

        public ReportsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IHostingEnvironment appEnvironment
        , FileUploadService fileUploadService)
        {
            _context = context;
            _userManager = userManager;
            _fileUploadService = fileUploadService;
            _appEnvironment = appEnvironment;
        }

        // GET: Reports
        public async Task<IActionResult> Index()
        {
            var reports = _context.Reports.Include(r => r.ApplicationUser).Include(r => r.Place);
            return View(await reports.ToListAsync());
        }

        // GET: Reports/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var report = await _context.Reports
                .Include(r => r.ApplicationUser)
                .Include(r => r.Place)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (report == null)
            {
                return NotFound();
            }

            return View(report);
        }


        public IActionResult Create()
        {
            ViewData["PlaceId"] = new SelectList(_context.Places, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(string title,int placeId, string description, double raiting)
        {
            Report report = new Report()
            {
                Title = title,
                Description = description,
                Raiting = raiting,
                PlaceId = placeId,
                ApplicationUserId = _userManager.GetUserId(User)
            };
            _context.Add(report);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var report = await _context.Reports.SingleOrDefaultAsync(m => m.Id == id);
            if (report == null)
            {
                return NotFound();
            }

            ViewData["PlaceId"] = new SelectList(_context.Places, "Id", "Id", report.PlaceId);
            return View(report);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,ApplicationUserId,PlaceId,Raiting")] Report report)
        {
            if (id != report.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    report.ApplicationUserId = _userManager.GetUserId(User);
                    _context.Update(report);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReportExists(report.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["PlaceId"] = new SelectList(_context.Places, "Id", "Name", report.PlaceId);
            return View(report);
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var report = await _context.Reports
                .Include(r => r.ApplicationUser)
                .Include(r => r.Place)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (report == null)
            {
                return NotFound();
            }

            return View(report);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var report = await _context.Reports.SingleOrDefaultAsync(m => m.Id == id);
            _context.Reports.Remove(report);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ReportExists(int id)
        {
            return _context.Reports.Any(e => e.Id == id);
        }
        public async Task<IActionResult> AddNewImage(int placeId, string userId, string title, IFormFile image)
        {
            if (image != null)
            {
                ReportImage img = new ReportImage()
                {
                    Title = title,
                    ApplicationUserId = userId,
                    PlaceId = placeId
                };
                UploadPhoto(img, image);
                _context.Images.Add(img);
                _context.SaveChanges();
            }

            
            return RedirectToAction("Index", "Places");
        }
        private void UploadPhoto(ReportImage image, IFormFile img)
        {
            var path = Path.Combine(_appEnvironment.WebRootPath, $"images\\{image.Title}\\image");
            _fileUploadService.Upload(path, img.FileName, img);
            image.ImagePath = $"images/{image.Title}/image/{img.FileName}";
        }
    }
}
