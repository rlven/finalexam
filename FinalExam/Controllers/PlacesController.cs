﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FinalExam.Data;
using FinalExam.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using FinalExam.Services;

namespace FinalExam.Controllers
{
    [Authorize]
    public class PlacesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _appEnvironment;
        private readonly FileUploadService _fileUploadService;

        public PlacesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IHostingEnvironment appEnvironment
        , FileUploadService fileUploadService)
        {
            _context = context;
            _userManager = userManager;
            _fileUploadService = fileUploadService;
            _appEnvironment = appEnvironment;
        }

        public async Task<IActionResult> Index(string searchWord, int pageNum=1)
        {
            var plaecs = _context.Places.Include(p => p.ApplicationUser).Include(p=>p.Reports).Include(p=>p.Images);
            if (!string.IsNullOrEmpty(searchWord))
            {
                plaecs = _context.Places.Where(p => p.Name.Contains(searchWord)).Include(p=>p.ApplicationUser).Include(p => p.Reports).Include(p => p.Images);
            }

            IndexViewModel ivm = new IndexViewModel();
            int pageSize = 20;

            var count = plaecs.Count();
            var items = await plaecs.Skip((pageNum - 1) * pageSize).Take(pageSize).ToListAsync();
            PageViewModel pageViewModel = new PageViewModel(count, pageNum, pageSize);

            ivm.PageViewModel = pageViewModel;
            ivm.Places = items;
            
            return View(ivm);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var place = await _context.Places
                .Include(p => p.ApplicationUser)
                .Include(p=>p.Reports)
                .Include(p=>p.Images)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (place == null)
            {
                return NotFound();
            }

            if (place.Reports.Count() != 0)
            {
                place.AverageRaiting = place.Reports.Average(s => s.Raiting);
            }

            return View(place);
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Desription,ApplicationUserId,ImagePath,AverageRaiting")] Place place, IFormFile image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    UploadPhoto(place, image);
                }

                place.ApplicationUserId = _userManager.GetUserId(User);
                _context.Add(place);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(place);
        }

        // GET: Places/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var place = await _context.Places.SingleOrDefaultAsync(m => m.Id == id);
            if (place == null)
            {
                return NotFound();
            }

            return View(place);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Desription,ApplicationUserId,ImagePath,AverageRaiting")] Place place)
        {
            if (id != place.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    place.ApplicationUserId = _userManager.GetUserId(User);;
                    _context.Update(place);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlaceExists(place.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View(place);
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var place = await _context.Places
                .Include(p => p.ApplicationUser)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (place == null)
            {
                return NotFound();
            }

            return View(place);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var place = await _context.Places.SingleOrDefaultAsync(m => m.Id == id);
            _context.Places.Remove(place);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlaceExists(int id)
        {
            return _context.Places.Any(e => e.Id == id);
        }
        private void UploadPhoto(Place place, IFormFile image)
        {
            var path = Path.Combine(_appEnvironment.WebRootPath, $"images\\{place.Name}\\image");
            _fileUploadService.Upload(path, image.FileName, image);
            place.ImagePath = $"images/{place.Name}/image/{image.FileName}";
        }
    }
}
